import math


class Triangle:

    def __init__(self):
        self.points = []

    def add_point(self, x, y):
        self.points.append((x, y))

    def perimeter(self):
        A = math.sqrt(math.pow((self.points[0][0]-self.points[1][0]), 2) + math.pow(
            (self.points[0][1]-self.points[1][1]), 2))
        B = math.sqrt(math.pow((self.points[1][0]-self.points[2][0]), 2) + math.pow(
            (self.points[1][1]-self.points[2][1]), 2))
        C = math.sqrt(math.pow((self.points[0][0]-self.points[2][0]), 2) + math.pow(
            (self.points[0][1]-self.points[2][1]), 2))
        return A+B+C

    def is_equal(self, t2):
        if self.points[0][0] != t2.points[0][0] or self.points[0][1] != t2.points[0][1] or self.points[1][1] != t2.points[1][1] or self.points[1][2] != t2.points[1][2] or self.points[2][1] != t2.points[2][1] or self.points[2][0] != t2.points[2][0] or self.points[1][0] != t2.points[1][0]:
            return True
        else:
            return False
    # def __eq__(self,t2):
    #     if self.points[0][0] != t2.points[0][0] or self.points[0][1] != t2.points[0][1] or self.points[1][1] != t2.points[1][1] or self.points[1][2] != t2.points[1][2] or self.points[2][1] != t2.points[2][1] or self.points[2][0] != t2.points[2][0] or self.points[1][0] != t2.points[1][0]:
    #         return True
    #     else:
    #         return False



def main():
    t1 = Triangle()
    t2 = Triangle()
    t3 = Triangle()
    t1.add_point(0, 0)
    t1.add_point(0, 3)
    t1.add_point(4, 0)
    t2.add_point(1, 2)
    t2.add_point(2, 1)
    t2.add_point(1, 5)
    t3.add_point(1, 2)
    t3.add_point(2, 1)
    t3.add_point(1, 5)
    Peri_Data1 = t1.perimeter()
    Peri_Data2 = t2.perimeter()
    print("Perimeter of the t1 object trinagle :-", Peri_Data1)
    print("Perimeter of the t2 object trinagle :-", Peri_Data2)

    Equal1 = t1.is_equal(t2)

    if Equal1 == 1:
        print("T1 and T2 are not equal")
    else:
        print("T1 and T2 are equal")

    Equal2 = t1.is_equal(t3)

    if Equal2 == 1:
        print("T1 is not Equal to T3")
    else:
        print("T1 is Equal to T3")

    Equal3 = t3.is_equal(t1)

    if Equal3 == 1:
        print("T3 is not equal to T1")
    else:
        print("T3 is equal to T1")
    Equal4 = t1.__eq__(t3)

    if Equal4 == 1:
        print("t1 is not equal to t3")
    else:
        print("t1 is equal to t3")


if __name__ == "__main__":
    main()
